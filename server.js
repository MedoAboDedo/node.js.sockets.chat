var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
app.get('/jquery.playSound.js', function(req, res){
  res.sendFile(__dirname + '/jquery.playSound.js');
});
app.get('/facebook.mp3', function(req, res){
  res.sendFile(__dirname + '/facebook.mp3');
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
	console.log(msg);
    io.emit('chat message', msg);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
